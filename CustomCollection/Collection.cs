﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace CustomCollection
{
    public class Collection<T> : IEnumerable<T>, INotifyCollectionChanged
    {
        private readonly List<T> _sourceList;

        public Collection()
        {
            _sourceList = new List<T>();
        }

        public T this[int index]
        {
            set
            {
                if (value == null)
                    throw new ArgumentException($"{value} can not be null");
                _sourceList[index] = value;
                CollectionChanged?.Invoke(this,
                    new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace));
            }
            get => _sourceList[index];
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _sourceList.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        public void Add(T item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));
            _sourceList.Add(item);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
        }

        public bool TryRemove(T item)
        {
            var index = _sourceList.IndexOf(item);
            var result = _sourceList.Remove(item);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, index));
            return result;
        }

        public bool TryRemoveAt(int index)
        {
            if (index < 0)
                return false;
            if (index > _sourceList.Count || index < _sourceList.Count)
                return false;
            var removableElement = _sourceList[index];
            _sourceList.RemoveAt(index);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, removableElement, index));
            return true;
        }

        private void OnCollectionChanged(NotifyCollectionChangedEventArgs eventArgs)
        {
            CollectionChanged?.Invoke(this, eventArgs);
        }
    }
}